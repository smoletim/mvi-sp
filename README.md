# Natural Language Processing with Disaster Tweets

Kaggle link: https://www.kaggle.com/competitions/nlp-getting-started/

## Repository structure
File report.pdf contains the report with all of the necessary description, information about data and results.

Files train.csv and test.csv contain the training and testing data as downloaded from kaggle.

Jupyter notebook mvi-sp-smoletim.ipynb contains all of the relevant code. This code has been used to produce the results mentioned in the report. It contains code for training as well as testing of the models. Ensembling is also included in this code.

Jupyter notebook mvi_semestralka_milestone.ipynb contains the old version of code, which was submitted for the milestone.

Image kaggle_leaderboard_screenshot.png contains screenshot of the leaderboard of kaggle competition with our submission.

## Code description & How to run
We have prepared a jupyter notebook, which train and evaluate the model for us and prepares the submission for kaggle. The solution was developed using google colab and so it attempts to mount to your google drive. Certain folder structure on your drive is expected if you want to use this notebook with combination of colab & google drive:
```
└── MVI_data
	├──models
	├──submissions
	├──train.csv
	├──test.csv
```

The csv files are your training and testing data. Models and submissions can be empty folders, where submission files and models will be saved.
For training, the only modifications that needs to be done before every run are the arguments in `args` dictionary at the beginning. Instance of the model is saved after each epoch (in case the validation loss is lower). To load the data and train the model run all of the cells up until the `Testing of the model` block.

To test the model, specify the name of the model in `model_name` variable. Do not include the whole path, just the name of the model, which can be read from the text output of the training process. The csv file in submission format will be saved in submissions directory.

To use ensemble of 2 models, run the `Ensemble` block. You need to define paths to the submission csvs in `csv1` and `csv2` variables. Your new submissions will be saved as ensemble.csv.